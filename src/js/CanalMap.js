	/* ------------------------------------------------ */
	// CanalMap
	/* ------------------------------------------------ */

	function CanalMap(parameters) {
		this.parameters = parameters;

		this.googleMap = parameters.googleMap;
		this.canalpaths = parameters.canals;

		this.canals = [];
		/*
		this.canals : {
			"points" : [] of [lat,lng],
			"pins" : [] of google.map.markers,
			"pl;yline" : google.maps.polyline object
			"title" : STRING
		}
		*/
		this.markers = [];
		this.polylines = [];

		this.pathIndex = 0;
		this.pathCount = 0;
		this.pointIndex = 0;
		this.pointCount = 0;

		this.addPointFlag = false;
		this.deletePointFlag = false;
		this.movePointFlag = false;

		this.init();
	}
	
	CanalMap.prototype = {
		"constructor" : CanalMap,

		"template" : function () { var that = this; },

		"init" : function () {

			// set default map centre and zoom
			// place marker at map centre, home

			let home = {"lat" : 51.5978579, "lng" : 0.0184724};

			this.googleMap.goto({
				"lat" : home.lat,
				"lng" : home.lng,
				"zoom" : 8
			});

			this.googleMap.placeMarkers([
				{
					"name" : "pinwhite",
					"caption" : "Home",
					"lat" : home.lat,
					"lng" : home.lng,
					"visible" : true
				}
			]);

			this.buildCanalPaths();
			this.setEvents();
			this.setControls();
		},

		"buildCanalPaths" : function () {
			console.log("CanalMap.buildCanalPaths");

			this.canalpaths.map(function(canal) {
				this.canals.push({
					"points" : canal,
					"pins" : [],
					"polyine" : null,
					"title" : "CANAL" 
				});	
			}, this);

			// let canalPolylinedata = [];
			this.canals.map(function(canal,index) {
				let canalColour = (index === 0) ? '#f00' : '#000';
				/*
				canalPolylinedata.push({
					"path" : canal.points,
					"colour" : canalColour
				});
				*/

				// build canal polyline
				canal.polyline = this.googleMap.definePolyLine({
					"path" : canal.points,
					"colour" : canalColour
				});

				// build canal pins
				canal.pins = [];
				let newPins = [];
				canal.points.map(function(point) {
					newPins.push({
						"name" : "ring",
						"lat" : point.lat,
						"lng" : point.lng,
						"caption" : "pin",
						"visible" : false
					});
				}, this);
				canal.pins = this.googleMap.placeMarkers(newPins);
			}, this);
			// this.polylines = this.googleMap.definePolyLines(canalPolylinedata);
		},

		"setEvents" : function() {

			let that = this;

			/*
			// add new points to map, single click to add new point
		    let pointIndex = 0;
		    let points = [];
		    let po = '';
			map.addListener('click', function(me) {
				// me = google.maps.MouseEvent
				// me.latLng = google.maps.LatLng
				// [ me.latLng.lat(), me.latLng.lng() ]

				let clickMarker = new google.maps.Marker({
					"position" : me.latLng,
					"map" : map,
					"title" : "index" + pointIndex
				});
				points.push(clickMarker);

				po = (po !== '') ? po + "," : po;
				po = po  + "{\"lat\" : " + me.latLng.lat() + ", \"lng\" : " + me.latLng.lng() + "}";
				$('#path').empty().append(po);

				console.log("index: %s start lat: %s lng: %s", pointIndex, me.latLng.lat(),me.latLng.lng());
				pointIndex++;
		    });
			*/

			// (pathplus||pathminus||newpath||savepath||showPins||linkpaths||pointplus||pointminus||addpoint||deletepoint||movepoint)

			// go to next path
			$('.pathplus').on('click',function(e) {
				e.preventDefault();

				that.nextPath();
			});

			// go to previous path
			$('.pathminus').on('click',function(e) {
				e.preventDefault();

				that.previousPath();
			});

			// go to next pin
			$('.pointplus').on('click',function(e) {
				e.preventDefault();

				that.nextPoint();
			});

			// go to previous pin
			$('.pointminus').on('click',function(e) {
				e.preventDefault();

				that.previousPoint();
			});
			
			$('.showPath').on('click',function(e) {
				e.preventDefault();

				/*
				let thisPathState = pathStates[pathIndex];
				thisPathState = !thisPathState;
				let newPathCaption = (thisPathState) ? 'ON' : 'OFF';

				$('#pathstate').text(newPathCaption);

				pathStates[pathIndex] = thisPathState;
				// paths[pathIndex].setVisible(pathStates[pathIndex]);
				*/
			});
		},

		"setControls" : function () {
			/*
			// centre map on route
			routeBounds = new google.maps.LatLngBounds();
			routeBounds.extend(this.startPoint);
			routeBounds.extend(this.endPoint);
			googleMap.fitBounds(routeBounds, 0);
			*/

			this.pathCount = this.canals.length;
			this.pointCount = this.canals[this.pathIndex].points.length;
			$('#pathcount').text(this.pathCount);
			$('#pointcount').text(this.pointCount);

			this.canals[this.pathIndex].pins[0].setVisible(true);
			this.updateControls();
		},

		"nextPath" : function () {
			// console.log("CanalMap.nextPath");

			if ( this.pathIndex < this.pathCount - 1 ) {
				this.canals[this.pathIndex].polyline.setOptions({"strokeColor" : "#000"});
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(false);
				this.pathIndex++;
				this.canals[this.pathIndex].polyline.setOptions({"strokeColor" : "#f00"});
				this.canals[this.pathIndex].pins[0].setVisible(true);
				this.resetPoint();
				this.updateControls();
			}
		},

		"previousPath" : function () {
			// console.log("CanalMap.previousPath");

			if ( this.pathIndex > 0 ) {

				this.canals[this.pathIndex].polyline.setOptions({"strokeColor" : "#000"});
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(false);
				this.pathIndex--;
				this.canals[this.pathIndex].polyline.setOptions({"strokeColor" : "#f00"});
				this.canals[this.pathIndex].pins[0].setVisible(true);
				this.resetPoint();
				this.updateControls();
			}
		},

		"nextPoint" : function () {
			// console.log("CanalMap.nextPoint");

			if ( this.pointIndex < this.pointCount - 1 ) {
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(false);
				this.pointIndex++;
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(true);
				this.updateControls();
			}
		},

		"previousPoint" : function () {
			// console.log("CanalMap.previousPoint");

			if ( this.pointIndex > 0 ) {
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(false);
				this.pointIndex--;
				this.canals[this.pathIndex].pins[this.pointIndex].setVisible(true);
				this.updateControls();
			}
		},

		"addPoint" : function () {
		},

		"deletePoint" : function () {
		},

		"movePoint" : function () {
			// https://stackoverflow.com/questions/3257401/toggle-dragging-of-google-map-markers-once-added-as-draggable
		},

		"newPath" : function () {
		},

		"savePath" : function () {
		},

		"showPath" : function () {
		},

		"hidePath" : function () {
		},

		"showPoint" : function () {
		},

		"hidePoint" : function () {
		},

		"setPoint" : function () {
		},

		"resetPoint" : function () {
			this.pointIndex = 0;
		},

		"updateControls" : function () {
			$('#pathindex').text(this.pathIndex + 1);
			$('#pointindex').text(this.pointIndex + 1);

			this.pointCount = this.canals[this.pathIndex].points.length;
			$('#pointcount').text(this.pointCount);

			this.googleMap.goto({
				"lat" : this.canals[this.pathIndex].points[this.pointIndex].lat,
				"lng" : this.canals[this.pathIndex].points[this.pointIndex].lng
			});
		}
		
	    /* ---------------------------------------------------------------------------------------------------------------- */

	}
	// window.CanalMap = CanalMap;
	export default CanalMap;
