import Queue from './Queue.js'

	/* ------------------------------------------------ */
	// GoogleRouter
	/* ------------------------------------------------ */

	function GoogleRouter(parameters) {
		this.parameters = parameters;

		this.googlemap = parameters.map;
		this.zones = parameters.zones;
		this.steps = parameters.steps;
		this.delay = parameters.delay;

		this.service = null;
		this.display = null;

		this.routeLength = 0;

		this.enteredULEZ2020 = false;
		this.enteredULEZ2019 = false;
		this.ULEZ2020entryPoint = {};
		this.ULEZ2019entryPoint = {};
		this.transitions = [];

		this.workSteps = [];

		this.init();
	}
	
	GoogleRouter.prototype = {
		"constructor" : GoogleRouter,

		"template" : function () { var that = this; },

		"init" : function () {
			console.log("GoogleRouter.init");

			this.service = new google.maps.DirectionsService;
			this.display = new google.maps.DirectionsRenderer({ "suppressMarkers" : true });
		},

		"routeTest" : function (journey) {
			let that = this;

			console.log("GoogleRouter.routeTest");

			this.journey = journey;

			$.ajax({
				"type" : "GET",
				"url" : "dist/json/path.json",
				"data" : {
				},
				"dataType" : "text",
				"cache" : false,
				"success" : function (text) {
					console.log('success');

					let response = $.parseJSON(text);

					// massage JSON test data so that it closely resembles the google.directions query results
					// response.routes[0].legs[0].distance.value
					// response.routes[0].overview_path

					// build array of latLng objects
					let gPoints = [];
					response.points.map(function(point,index){
						gPoints.push(new google.maps.LatLng( point.lat, point.lng ));
					});

					let data = {
						"routes" : [
							{
								"overview_path" : gPoints,
								"legs" : [
									{
										"distance" : {
											"value" : response.distance
										}
									}
								]
							}
						]
					};

					that.originalResponse = data;
					that.processResponse(journey,data);
				},
				"complete" : function () {
					console.log('complete');
				}
			});
		},

		"route" : function (journey) {
			let that = this;

			console.log("GoogleRouter.route");

			this.journey = journey;

			// query oggole directions API
			this.service.route({
				"origin" : journey.origin,
				"destination" : journey.destination,
				"travelMode" : "DRIVING",
				"language" : "en"
			},
			function(response, status) {
				if (status === 'OK') {
					that.originalResponse = response;
					that.processResponse(journey,response);
				} else {
					// inability to find route
					that.error(status);
				}
			});

		},

		"processResponse" : function (journey,response) {
			let that = this;

			console.log("GoogleRouter.processResponse");

			this.routeLength = response.routes[0].legs[0].distance.value;

			this.workingRouteNodes = this.cloneArray(response.routes[0].overview_path);
			console.log("route length: %skm nodes: %s", (this.routeLength / 1000), this.workingRouteNodes.length);

			// save path data in JSON file for testing
			// this.archivePath(this.workingRouteNodes);

			// return;

			let polyLinePath = [];
			this.workingRouteNodes.map(function(point){
				polyLinePath.push({
					"lat" : point.lat(),
					"lng" : point.lng()
				});
			});

			polyLinePath = this.assignPointsToZones(polyLinePath);

			// prepare reduced point list for the journey animation
			this.reducedNodes = this.reduceRouteNodes( polyLinePath );

			/* */
			// build route segments
			let lastNodeIndex = 0, endIndex = 1, nodeCount = this.reducedNodes.length, segmentStart = 0, segmentEnd = 0, lineSegment = [];
			do {
				segmentStart = this.reducedNodes[lastNodeIndex].index;
				segmentEnd = this.reducedNodes[endIndex].index;
				lineSegment = polyLinePath.slice(segmentStart, (segmentEnd + 1));

				// console.log("node: %s index: %s node: %s index: %s", lastNodeIndex, segmentStart, endIndex, segmentEnd);

				this.workSteps.push({
					"point" : this.reducedNodes[lastNodeIndex],
					"range" : {
						"min" : segmentStart,
						"max" : (segmentEnd + 1)
					},
					"journey" : lineSegment
				});

				lastNodeIndex = endIndex;
				endIndex++;
			} while (endIndex < nodeCount)

			// draw reduced journey points

			let superThat = this;
			let queueParameters = {
				"queue" : this.workSteps, // create clone of subset array
				"worker" : {
					"global" : {
						"title" : journey.title,
						"delay" : superThat.delay
					},
					"action" : function(){
						let that = this;
						let delay = window.setTimeout(function(){
							let workItem = that.settings.item;

							// superThat.googlemap._map.setCenter( new google.maps.LatLng( workItem.point.lat, workItem.point.lng ) );

							let pinName = '';
							switch(workItem.point.zone) {
								case "ulez2019" : { pinName = 'pinred' ;} break;
								case "ulez2020" : { pinName = 'pinwhite' ;} break;
								default : { pinName = 'pinblack' ;}
							}

							if(workItem.point.index === 0) {
								pinName = 'pingreen';
							}

							/*
							superThat.googlemap.placeMarkers([{
								"name" : pinName,
								"lat" : workItem.point.lat,
								"lng" : workItem.point.lng,
								"caption" : "distance: " + Number( workItem.point.pd / 1000 ).toFixed(2) + "km",
								"visible" : true
							}]);
							*/

							// plot route segment
							// set route colour based on whether it is inside or outside the ULEZ2020
							// entry lies within current range - split route into two segments, one outside, one inside
							// entry lies outside current range
							// min < ULEZ2020 entry point outside - blue
							// min > ULEZ2020 entry point inside - red
							let routeColour = "#0000FF";

							if (workItem.range.min < superThat.ULEZ2020entryPoint.index && superThat.ULEZ2020entryPoint.index < workItem.range.max) {
								// entry lies within current range - split route into two segments, one outside, one inside

								// plot marker on entry point
								superThat.googlemap.placeMarkers([{
									"name" : "pinblack",
									"lat" : superThat.ULEZ2020entryPoint.point.lat(),
									"lng" : superThat.ULEZ2020entryPoint.point.lng(),
									"caption" : "journey enters extendex ULEZ",
									"visible" : true
								}]);

								// split journey on workItem.journey[n] + min == superThat.ULEZ2020entryPoint.index
								let splitPoint = superThat.ULEZ2020entryPoint.index - workItem.range.min
								let a = workItem.journey.slice(0, (splitPoint + 1));
								let b = workItem.journey.slice(splitPoint, workItem.journey.length);

								let routePolylineA = superThat.googlemap.definePolyLine({
									"path" : a,
									"geodesic" : true,
									"strokeColor" : "#0000FF",
									"strokeOpacity" : 0.75,
									"strokeWeight" : 2
								});

								let routePolylineB = superThat.googlemap.definePolyLine({
									"path" : b,
									"geodesic" : true,
									"strokeColor" : "#FF0000",
									"strokeOpacity" : 0.75,
									"strokeWeight" : 2
								});

							} else {
								// entryn point lies outside this segment
								routeColour = (workItem.range.min > superThat.ULEZ2020entryPoint.index) ? "#FF0000" : "#0000FF";
								let routePolyline = superThat.googlemap.definePolyLine({
									"path" : workItem.journey,
									"geodesic" : true,
									"strokeColor" : routeColour,
									"strokeOpacity" : 0.75,
									"strokeWeight" : 2
								});
							}

							that.completed();	
						}, that.settings.global.delay);
					},
					"callback" : function(){
						this.queue.workerCompleted();
					}
				},
				"started" :  function(){
					console.log('queue started');

					// superThat.googlemap._map.setCenter( superThat.journey.origin );
					// superThat.googlemap._map.setZoom(14);

					// plot journey start
					superThat.googlemap.placeMarkers([{
						"name" : "pingreen",
						"lat" : superThat.journey.origin.lat(),
						"lng" : superThat.journey.origin.lng(),
						"caption" : "Start",
						"visible" : true
					}]);

				},
				"finished" :  function(){
					// destination pin
					let endpin = superThat.reducedNodes[superThat.reducedNodes.length - 1];
					let pinName = '';

					/*
					switch(endpin.zone) {
						case "ulez2019" : { pinName = 'pinred' ;} break;
						case "ulez2020" : { pinName = 'pinwhite' ;} break;
						default : { pinName = 'pinblack' ;}
					}
					*/
					pinName = 'pinred';

					superThat.googlemap.placeMarkers([{
						"name" : pinName,
						"lat" : endpin.lat,
						"lng" : endpin.lng,
						"caption" : "destination: " + journey.title,
						"visible" : true
					}]);
				}
			};
			this.queue = new Queue(queueParameters);
			this.queue.startWorker();
			/* */

// this.plotReducedRouteNodes(this.reducedNodes);

// this.plotTransitionPoints(polyLinePath);

/*
// draw route using full path
let routePolyline = this.googlemap.definePolyLine({
	"path" : polyLinePath,
	"geodesic" : true,
	"strokeColor" : '#0000FF',
	"strokeOpacity" : 0.75,
	"strokeWeight" : 2
});
*/

/*
// draw lines between each journey point, approximation of route
let routePolyline2 = this.googlemap.definePolyLine({
	"path" : this.reducedNodes,
	"geodesic" : true,
	"strokeColor" : '#0000FF',
	"strokeOpacity" : 0.75,
	"strokeWeight" : 2
});
*/
		},

		"assignPointsToZones" : function (path) {
			let that = this;

			console.log("GoogleRouter.assignPointsToZones");

			let previousPoint = this.journey.origin,
				ptpDistance = 0,
				totalDistance = 0,
				workpoint,
				node1 = previousPoint,
				zone1 = {"zone" : "normal", "index" : 0 },
				zone2,
				transitions = [];

			// iterate through all points in journey
			// stored as simple javascript objects {"lat" : LAT, "lng" : LNG} not google LatLng objects

			path.map(function(point,index){
				workpoint = new google.maps.LatLng( point.lat, point.lng );

				// calculate distance from start to point
				ptpDistance = google.maps.geometry.spherical.computeDistanceBetween(previousPoint, workpoint);
				totalDistance = totalDistance + ptpDistance;
				point.ptp = ptpDistance;
				point.pd = totalDistance;

				// check to see which zone the point lies in
				// record the point at which the journey first enters the ULEZ2020
				let zone = 'normal';
				
				if( google.maps.geometry.poly.containsLocation(workpoint, this.zones.ulez2020) ) {
					zone = 'ulez2020';
					// record the point at which the journey enters the ULEZ2020
					if (!this.enteredULEZ2020) {
						this.enteredULEZ2020 = true;
						this.ULEZ2020entryPoint = {
							"point" : workpoint,
							"index" : index
						};
					}
				};

				/*
				if( google.maps.geometry.poly.containsLocation(workpoint,this.zones.ulez2019) ) {
					zone = 'ulez2019';
					// record the point at which the journey first enters the ULEZ2019
					if (!this.enteredULEZ2019) {
						this.enteredULEZ2019 = true;
						this.ULEZ2019entryPoint = {
							"point" : workpoint,
							"index" : index
						};
					}
				};
				*/
				
				point.zone = zone;

				zone2 = {"zone" : zone, "index" : index };
				if(zone1.zone !== zone2.zone){
					// change of zone
					transitions.push({"a" : zone1, "b" : zone2 });
				}
				zone1 = zone2;

				previousPoint = workpoint;
			}, this);

			this.transitions = transitions;
			return path;
		},

		/*
		currently based on number of steps/points in journey
		complicated journeys may have more steps per unit distance, and spacing of steps may vary
		may have to update to use distance so as to allow for even step spacing
		*/
		"reduceRouteNodes" : function (route) {
			let that = this;

			console.log("GoogleRouter.reduceRouteNodes");

			/*
			// reduce journey based on steps
			// extract N (this.steps) points along the route
			let reducePath = [];
			let nodeIndex = 0;
			let nodeCount = route.length;
			let lastNode = (nodeCount - 1);
			let nodeInterval = Math.floor(nodeCount / this.steps);
			// let nodeInterval = 1; // display all points

			do{
				let node = route[nodeIndex];

				reducePath.push({
					"lat" : node.lat,
					"lng" : node.lng,
					"zone" : node.zone,
					"index" : nodeIndex
				});
				nodeIndex = nodeIndex + nodeInterval;
			} while (nodeIndex < nodeCount)

			// include the destination point
			if(nodeIndex !== nodeCount) {
				let node = route[lastNode];

				reducePath.push({
					"lat" : node.lat,
					"lng" : node.lng,
					"zone" : node.zone,
					"index" : lastNode,
				});
			}
			*/

			// reduce journey based on distance
			let reducePath = [];
			let distanceInterval = this.routeLength / this.steps;
			let nodeIndex = 0;
			let nodeCount = route.length;
			let lastNode = (nodeCount - 1);
			let distanceWindow = distanceInterval;

			// add first step at start of route
			let node = route[0];
			reducePath.push({
				"lat" : node.lat,
				"lng" : node.lng,
				"zone" : node.zone,
				"pd" : node.pd,
				"index" : nodeIndex
			});

			do{
				let node = route[nodeIndex];

				if (node.pd > distanceWindow) {
					reducePath.push({
						"lat" : node.lat,
						"lng" : node.lng,
						"zone" : node.zone,
						"pd" : node.pd,
						"index" : nodeIndex
					});
					distanceWindow = distanceWindow + distanceInterval;
				}
				nodeIndex++;
			} while (nodeIndex < nodeCount)

			return reducePath;
		},

		"error" : function (error) {
			console.log("GoogleRouter.error: %s", error);
		},

	    "cloneArray" : function (array) {
	      let that = this,
	          ai = 0,
	          ac = array.length,
	          ta,
	          newArray = [],
	          newObject;

	      do {
	        ta = array[ai];

	        newObject = {};

	        Object.keys(ta).map(function(property){
	        	newObject[property] = ta[property];
	        });
	        newArray.push(newObject);

			/* */

	        ai++;
	      } while (ai < ac)

	      return newArray;
		},

	    "archivePath" : function (path) {

			let pathExport = [], pi = 0, pn = path[pi];
			do{
				pn = path[pi];
				pathExport.push({
					"lat" : pn.lat(),
					"lng" : pn.lng()
				})
				pi++;
			}while(pi < path.length)

			console.log('pathExport')
			console.log(pathExport);

			$.ajax({
				"type" : "POST",
				"url" : "dist/php/save.php",
				"data" : {
					"path" : {
						"title" : "test data: home to STACK",
						"start" : {"lat" : 51.5218891, "lng" : -0.1359421 },
						"end" : { "lat" : 51.59772129317902, "lng" : 0.018531116087388 },
						"distance" : this.routeLength,
						"pointcount" : pathExport.length,
						"points" : pathExport
					}
				},
				"dataType" : "text",
				"cache" : false,
				"success" : function (text) {
					console.log('success');
					let response = $.parseJSON(text);
					console.log(response);
				},
				"complete" : function () {
					console.log('complete');
				}
			});
		},

		"plotTransitionPoints" : function (polyLinePath) {
			let that = this;

			let newPins = [];
			this.transitions.map(function(transition,index){
				newPins.push({
					"name" : "pinwhite",
					"lat" : polyLinePath[transition.a.index].lat,
					"lng" : polyLinePath[transition.a.index].lng,
					"caption" : "home",
					"visible" : true
				});
				newPins.push({
					"name" : "pinblack",
					"lat" : polyLinePath[transition.b.index].lat,
					"lng" : polyLinePath[transition.b.index].lng,
					"caption" : "STACK",
					"visible" : true
				});
			},this);
			that.googlemap.placeMarkers(newPins);
		},

		"plotJourneyPoints" : function (journey) {
			let that = this;

			let newPins = [];
			newPins.push({
				"name" : "pingreen",
				"lat" : journey.origin.lat(),
				"lng" : journey.origin.lng(),
				"caption" : "home",
				"visible" : true
			});
			newPins.push({
				"name" : "pinred",
				"lat" : journey.destination.lat(),
				"lng" : journey.destination.lng(),
				"caption" : "STACK",
				"visible" : true
			});
			that.googlemap.placeMarkers(newPins);
		},

		"plotFullRoute" : function (route) {
			// show route on map
			this.display.setMap(this.googlemap._map);
			this.display.setDirections(route);
		},

		"plotReducedRouteNodes" : function (route) {
			let that = this;

			let newPins = [];
			route.map(function(point,index){
				// check to see if point lies within ulez2020/ulez2019
				let pinName = 'pinblack';
				let caption = '';
				let zone = point.zone;
				switch(zone) {
					case "ulez2019" : { pinName = 'pinred' ;} break;
					case "ulez2020" : { pinName = 'pinwhite' ;} break;
					default : { pinName = 'pinblack' ;}
				}

				newPins.push({
					"name" : pinName,
					"lat" : point.lat,
					"lng" : point.lng,
					"caption" : "index: " + point.index,
					"visible" : true
				});
			},this);

			that.googlemap.placeMarkers(newPins);
		}

	    /* ---------------------------------------------------------------------------------------------------------------- */

	}
	// window.GoogleRouter = GoogleRouter;
	export default GoogleRouter;
