import Queue from './Queue.js'

/* ------------------------------------------------ */
// GoogleRouter2
/* ------------------------------------------------ */

function GoogleRouter2(parameters) {
	this.parameters = parameters;

	this.controller = parameters.controller;
	this.map = parameters.map;
	this.zones = parameters.zones;
	this.steps = parameters.steps;
	this.delay = parameters.delay;

	this.init();
}

GoogleRouter2.prototype = {
	"constructor" : GoogleRouter2,

	"template" : function () { var that = this; },

	"init" : function () {
		if ( consoleLog ) {console.log("GoogleRouter2.init"); }
	},

	"route" : function (journey) {
		if ( consoleLog ) {console.log("GoogleRouter2.route - journey title: '%s' distance: %sm", journey.title, journey.distance); }

		this.title = journey.title;
		this.id = journey.id;
		this.origin = journey.origin;
		this.destination = journey.destination;
		this.distance = journey.distance;
		// this.nodes = journey.nodes;
		this.nodeCount = journey.nodes.length;

		this.ULEZ2020entryPoint = false;

		this.path = this.assignZonesToNodes(journey.nodes);
		// if ( consoleLog ) {console.log( this.path ); }
		// if ( consoleLog ) {console.log( this.transitions ); }
		
		if ( this.ULEZ2020entryPoint ) {
			this.controller.showWarningFlag = true;
			this.controller.showWarning(journey.field);
		}

		this.nodeSubset = this.reduceNodes(this.path);
		if ( consoleLog ) {console.log( this.nodeSubset ); }

		// break journey nodes into segments
		let lastNodeIndex = 0, endIndex = 1, nodeCount = this.nodeSubset.length, segmentStart = 0, segmentEnd = 0, lineSegment = [];
		let workSteps = [];
		do {
			segmentStart = this.nodeSubset[lastNodeIndex].index;
			segmentEnd = this.nodeSubset[endIndex].index;
			lineSegment = this.path.slice(segmentStart, (segmentEnd + 1));

			workSteps.push({
				"point" : this.nodeSubset[lastNodeIndex],
				"range" : {
					"min" : segmentStart,
					"max" : (segmentEnd + 1)
				},
				"segment" : lineSegment
			});

			lastNodeIndex = endIndex;
			endIndex++;
		} while (endIndex < nodeCount)

		/* */
		if (archiveJourneys) {
			// archive journey(s)
			let mpath = []
			this.path.map(function(node){
				mpath.push([node.lat,node.lng]);
			},this);
			this.archiveJourney({
				"title" : this.title,
				"id" : this.id,
				"origin" : this.origin,
				"destination" : this.destination,
				"distance" : this.distance,
				"nodecount" : this.nodeCount,
				"path" : mpath,
			});
		}
		/* */

		// draw segments in steps with delay between each segment
		let superThat = this;
		let drawQueueParameters = {
			"queue" : workSteps, // create clone of subset array
			"worker" : {
				"global" : {
					"title" : this.title,
					"delay" : superThat.delay
				},
				"action" : function(){
					let that = this;
					let delay = window.setTimeout(function(){
						let workItem = that.settings.item;

						if ( superThat.ULEZ2020entryPoint && (workItem.range.min < superThat.ULEZ2020entryPoint.index) && (superThat.ULEZ2020entryPoint.index < workItem.range.max) ) {
							// plot marker on entry point
							superThat.map.placeMarkers([{
								"name" : "pinred",
								"lat" : superThat.ULEZ2020entryPoint.point.lat(),
								"lng" : superThat.ULEZ2020entryPoint.point.lng(),
								"caption" : "journey enters extended ULEZ",
								"visible" : true
							}]);

							// break segment into 2 parts for segments that cross zone boundaries
							let splitPoint = superThat.ULEZ2020entryPoint.index - workItem.range.min
							let a = workItem.segment.slice(0, (splitPoint + 1));
							let b = workItem.segment.slice(splitPoint, workItem.segment.length);

							let routePolylineA = superThat.map.definePolyLine({
								"path" : a,
								"geodesic" : true,
								"strokeColor" : "#0000FF",
								"strokeOpacity" : 0.75,
								"strokeWeight" : 2
							});

							let routePolylineB = superThat.map.definePolyLine({
								"path" : b,
								"geodesic" : true,
								"strokeColor" : "#FF0000",
								"strokeOpacity" : 0.75,
								"strokeWeight" : 2
							});
							
						} else {
							// entry point lies outside this segment
							let routeColour = (workItem.range.min > superThat.ULEZ2020entryPoint.index) ? "#FF0000" : "#0000FF";
							let routePolyline = superThat.map.definePolyLine({
								"path" : workItem.segment,
								"geodesic" : true,
								"strokeColor" : routeColour,
								"strokeOpacity" : 0.75,
								"strokeWeight" : 2
							});
						}

						that.completed();	
					}, that.settings.global.delay);
				},
				"callback" : function(){
					this.queue.workerCompleted();
				}
			},
			"started" :  function(){
				/*
				// place pin at origin
				superThat.map.placeMarkers([{
					"name" : "pingreen",
					"lat" : superThat.origin.lat,
					"lng" : superThat.origin.lng,
					"caption" : "Start",
					"visible" : true
				}]);
				*/
			},
			"finished" :  function(){
				/*
				// place pin at destination point
				superThat.map.placeMarkers([{
					"name" : "pinblack",
					"lat" : superThat.destination.lat,
					"lng" : superThat.destination.lng,
					"caption" : superThat.title,
					"visible" : true
				}]);
				*/
			}

		};
		this.drawQueue = new Queue(drawQueueParameters);
		this.drawQueue.startWorker();
	},

	"assignZonesToNodes" : function (nodes) {
		if ( consoleLog ) {console.log("GoogleRouter2.assignZonesToNodes"); }
		let that = this;

		let path = [];
		let origin = new google.maps.LatLng( this.origin.lat, this.origin.lng ),
			p2pd = 0,
			wd = 0,
			previousPoint = origin,
			currentPoint = null,
			zone1 = {"zone" : "normal", "index" : 0 },
			zone2,
			transitions = [];


		nodes.map(function(point, index){
			// point = latLng -> latLng.lat(), latLng.lng()

			// add distance from start to each point
			p2pd = google.maps.geometry.spherical.computeDistanceBetween(previousPoint, point);
			wd = wd + p2pd;

			// check to see which zone the point is in
			let zone = 'normal';

			// record when journey enters ULEZ2020			
			if( google.maps.geometry.poly.containsLocation(point, this.zones.ulez2020) ) {
				zone = 'ulez2020';
				// record the point at which the journey enters the ULEZ2020
				if (!this.enteredULEZ2020) {
					this.enteredULEZ2020 = true;
					this.ULEZ2020entryPoint = {
						"point" : point,
						"index" : index
					};
				}
			};

			// record transitions between zones
			zone2 = {"zone" : zone, "index" : index };
			if(zone1.zone !== zone2.zone){
				// change of zone
				transitions.push({"a" : zone1, "b" : zone2 });
			}
			zone1 = zone2;

			previousPoint = point;

			path.push({
				"lat" : point.lat(),
				"lng" : point.lng(),
				"p2pd" : p2pd,
				"pd" : wd,
				"zone" : zone,
				"pin" : "pinblack"
			});

		}, this);

		this.transitions = transitions;
		return path;
	},

	"reduceNodes" : function (nodes) {
		if ( consoleLog ) {console.log("GoogleRouter2.reduceNodes"); }

		// reduce journey based on distance
		let distanceInterval = this.distance / this.steps;
		if ( consoleLog ) {console.log("distanceInterval : %s", distanceInterval); }

		let nodeSubset = [];
		let nodeIndex = 0;
		let nodeCount = nodes.length;
		let lastNode = (nodeCount - 1);
		let distanceWindow = distanceInterval;

		// add first step at start of route
		let node = nodes[0];
		nodeSubset.push({
			"lat" : node.lat,
			"lng" : node.lng,
			"zone" : node.zone,
			"pd" : node.pd,
			"index" : nodeIndex
		});

		/* */
		// reduce number of nodes in journey
		do{
			let node = nodes[nodeIndex];

			if (node.pd > distanceWindow) {
				nodeSubset.push({
					"lat" : node.lat,
					"lng" : node.lng,
					"zone" : node.zone,
					"pd" : node.pd,
					"index" : nodeIndex
				});
				distanceWindow = distanceWindow + distanceInterval;
			}
			nodeIndex++;
		} while (nodeIndex < nodeCount)
		/* */

		/*
		// map nodes directly into nodeSubset without reduction
		nodes.map(function(node,nodeIndex){
			nodeSubset.push({
				"lat" : node.lat,
				"lng" : node.lng,
				"zone" : node.zone,
				"pd" : node.pd,
				"index" : nodeIndex
			});
		});		
		*/

		// finish off journey - fill in nodes between last subset node and destination
		let lastSubsetNode = nodeSubset[nodeSubset.length - 1];
		let lastSubsetNodeIndex = lastSubsetNode.index;
		let endSubset = nodes.slice(lastSubsetNodeIndex);

		endSubset.map(function(node){
			nodeSubset.push({
				"lat" : node.lat,
				"lng" : node.lng,
				"zone" : node.zone,
				"pd" : node.pd,
				"index" : nodeIndex
			});
		},this);

		return nodeSubset;
	},

	"archiveJourney" : function (journey) {
		if ( consoleLog ) {console.log("GoogleRouter2.archiveJourney"); }

		$.ajax({
			"type" : "POST",
			"url" : "dist/php/save2.php",
			"data" : {
				"journey" : journey
			},
			"dataType" : "text",
			"cache" : false,
			"success" : function (text) {
				if ( consoleLog ) { console.log("success"); }
				let response = $.parseJSON(text);

				if ( consoleLog ) { console.log( response.title ); }
			},
			"complete" : function () {
				if ( consoleLog ) { console.log("complete"); }
			}
		});
	}

    /* ---------------------------------------------------------------------------------------------------------------- */

}
// window.GoogleRouter2 = GoogleRouter2;
export default GoogleRouter2;
