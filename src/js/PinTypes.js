let PinTypes = [
	{
		"name" : "ring",
		"size" : {"width" : 20, "height" : 20},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 10},
		"path" : "dist/img/pins/ring.png"
	},
	{
		"name" : "pinblack",
		"size" : {"width" : 20, "height" : 34},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 34},
		"path" : "dist/img/pins/pin.png"
	},
	{
		"name" : "pinwhite",
		"size" : {"width" : 20, "height" : 34},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 34},
		"path" : "dist/img/pins/pin-white.png"
	},
	{
		"name" : "pinred",
		"size" : {"width" : 20, "height" : 34},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 34},
		"path" : "dist/img/pins/pin-red.png"
	},
	{
		"name" : "pingreen",
		"size" : {"width" : 20, "height" : 34},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 34},
		"path" : "dist/img/pins/pin-green.png"
	},
	{
		"name" : "pinblue",
		"size" : {"width" : 20, "height" : 34},
		"position" : {"x" : 0, "y" : 0},
		"focus" : {"x" : 10, "y" : 34},
		"path" : "dist/img/pins/pin-blue.png"
	}
];

export default PinTypes;	