import Worker from './Worker.js'

/* ------------------------------------------------ */
// Queue
/* ------------------------------------------------ */

function Queue(parameters) {
	this.parameters = parameters;
	this.queue = parameters.queue;
	this.worker = parameters.worker;
	this.started = parameters.started;
	this.finished = parameters.finished;

	this.queueLimit = this.queue.length;
	this.workerBox = [];
	this.workerRunning = false;

	this.init();
}

Queue.prototype = {
	"constructor" : Queue,
	"template" : function () {var that = this; },
	
	"init" : function () {
		if ( consoleLog ) {console.log("Queue.init"); }
		var that = this;

		if (this.started !== null && typeof this.started === 'function' ) {
			this.started();
		}
	},
	"startWorker" : function () {
			if ( consoleLog ) {console.log("Queue.startWorker"); }
			var that = this,
			newItem,
			newWorker;

		if ( this.queueLimit > 0 ) {
			// newItem = this.queue.pop(); // get LAST element in queue
			newItem = this.queue.splice(0,1); // get FIRST item in queue

			this.queueLimit = this.queue.length;

			this.workerRunning = true;

			newWorker = new Worker({
				"queue" : this,
				"settings" : {
					"global" : that.worker.global,
					"item" : newItem[0]
				},
				"action" : that.worker.action,
				"callback" : that.worker.callback
			});
		}
	},
	"workerCompleted" : function () {
		if ( consoleLog ) {console.log("Queue.workerCompleted"); }
		var that = this;

		if ( consoleLog ) {console.log("this.queueLimit = %s", this.queueLimit); }
		
		/* logic for dealing with consecutive single jobs */
		if ( this.queueLimit > 0 ) {
			this.startWorker();
		} else {
			this.queueCompleted();
		}

		/*
		// are there more jobs in the queue?
		if ( this.queueLimit > 0 ) {
			// start next job
			this.startWorker();
		} else {
			// queue complete
			// this.queueCompleted();
		}

		if ( !this.workerRunning) {
			this.queueCompleted();
		}

		if ( this.queueLimit == 0 && this.workerRunning) {
			this.workerRunning = false;
		}
		*/
	},
	"queueCompleted" : function () {
		if ( consoleLog ) {console.log("Queue.queueCompleted"); }
		var that = this;

		if (this.finished !== null && typeof this.finished === 'function' ) {
			this.finished();
		}

	}
};

// window.Queue = Queue;
export default Queue;