import Queue from './Queue.js'
import GoogleMap from './GoogleMap.js'

import GoogleRouter from './GoogleRouter.js'
// requires /src/js/Queue requires /src/js/Worker
import GoogleRouter2 from './GoogleRouter2.js'
// requires /src/js/Queue requires /src/js/Worker

import ulez2019path from './ulez2019path'
import ulez2020path from './ulez2020path'
import PinTypes from './PinTypes'

/* ------------------------------------------------ */
// ULEZControl
/* ------------------------------------------------ */

function ULEZControl(parameters) {
	this.parameters = parameters;

	this.google = parameters.google;
	this.apikey = parameters.apikey;
	this.container = parameters.container;
	this.segmentCount = parameters.segmentCount;
	this.segmentInterval = parameters.segmentInterval;
	this.destinationCount = parameters.destinationCount;

	this.origin = false;
	this.originReady = false;
	this.destinations = [];
	this.destinationsReady = [];
	this.journeys = [];
	this.routes = [];

	this.googleMap = null;
	this.ulez2019polygon = null;
	this.ulez2020polygon = null;
	this.obscure = null;
	this.ulezRoute = null;

	this.showWarningFlag = false;

	this.init();
}

ULEZControl.prototype = {
	"constructor" : ULEZControl,

	"template" : function () { var that = this; },

	"init" : function () {
		if ( consoleLog ) { console.log("ULEZControl.init"); } 
	},

	"drawMap" : function (settings) {
		if ( consoleLog ) {console.log("ULEZControl.drawMap"); }
		let google = this.google;
		let that = this;

		this.googleMap = new GoogleMap({
			"APIKey" : this.apikey,
			"mapcontainer" : this.container,
			"defaultSettings" : {
				/* "styles" : [
				  {
				    "featureType": "road",
				    "stylers": [
				      {
				        "color": "#ff0000"
				      }
				    ]
				  }
				], */
				"zoom" : settings.zoom,
				"center" : new google.maps.LatLng( settings.center.lat, settings.center.lng ),
				"panControl" : true,
				"zoomControl" : true,
				"mapTypeControl" : false,
				"streetViewControl" : false,
				"clickableIcons" : false,
				"mapTypeControlOptions": {
					"mapTypeIds": [ 'roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map' ]
				}
			}
		});
		this.googleMap.defineCustomMarkers(PinTypes);
		this.directionsService = new google.maps.DirectionsService;


	},

	"bindAutocompleteFields" : function () {
		if ( consoleLog ) {console.log("ULEZControl.bindAutocompleteFields"); }
		let that  = this;

		// autocomplete fields for origin and 3 places
		// ['geocode', 'address', 'establishment']
		// [] = all
		let types = [];
		let options = { "types" : types, "language" : "en"};

		let originAutocomplete = new google.maps.places.Autocomplete( document.querySelector('input#origin'), options );
		originAutocomplete.addListener('place_changed', function(){
			that.placeChange( this,'origin');
		});

		let place1Autocomplete = new google.maps.places.Autocomplete( document.querySelector('input#place1'), options );
		place1Autocomplete.addListener('place_changed', function(){
			that.placeChange( this,'place1');
		});

		let place2Autocomplete = new google.maps.places.Autocomplete( document.querySelector('input#place2'), options );
		place2Autocomplete.addListener('place_changed', function(){
			that.placeChange( this,'place2');
		});

		let place3Autocomplete = new google.maps.places.Autocomplete( document.querySelector('input#place3'), options );
		place3Autocomplete.addListener('place_changed', function(){
			that.placeChange( this,'place3');
		});
	},

	"placeChange" : function (argon,source) {
		if ( consoleLog ) {console.log("ULEZControl.placeChange"); }
		let that  = this;

		try{
			let response = argon.getPlace();
			if(response !== null && typeof response === "object") {
				let newPlace = response.geometry.location;

				if ( consoleLog ) {console.log( "name: %s source: %s lat: %s lng: %s", response.name, source, newPlace.lat(), newPlace.lng() ); }

				// if origin has been defined
				// check to see if destination is in UK and route exists between origin and destination
				// if route is valid, store and process later
				// if origin hasn't been set yet just check to see if destination is in UK / display message requesting user selects origin before selecting destinations

				switch( source ) {
					case 'origin' : {
						this.origin = {
							"lat" : newPlace.lat(),
							"lng" : newPlace.lng(),
							"caption" : response.name,
							"field" : source
						};
						this.originReady = true;
						this.warningNoOrigin( false );
					} break;
					default : {
						// store destination
						this.destinations[source] = {
							"lat" : newPlace.lat(),
							"lng" : newPlace.lng(),
							"caption" : response.name,
							"field" : source
						};
						this.destinationsReady.push(source);
						this.warningNoDestinations( false );
					} break;
				}

				// mark field
				$('#' + source).parents('div.formField').eq(0).addClass('ready');

				// drop pin on map to mark destination
				let pintype = (source === 'origin') ? 'pingreen' : 'pinred';
				that.googleMap.placeMarkers([{
					"name" : pintype,
					"lat" : newPlace.lat(),
					"lng" : newPlace.lng(),
					"caption" : response.name,
					"visible" : true
				}]);
				that.googleMap._map.setCenter( newPlace );
				
			} else {

			}
		}
		catch (e) {
			console.log(e);
		}
	},

	"generateULEZ2019" : function () {
		if ( consoleLog ) {console.log("ULEZControl.drawULEZ2019"); }
		// let google = this.google;

		// current London ultra low emission zone
		this.ulez2019polygon = this.googleMap.definePolygon({
			"path" : ulez2019path,
			"strokeColor" : '#ff0000',
			"strokeOpacity" : 0.5,
			"strokeWeight" : 0,
			"fillColor" : '#ff0000',
			"fillOpacity" : 0.35
		});
	},

	"generateULEZ2020" : function () {
		if ( consoleLog ) {console.log("ULEZControl.drawULEZ2019"); }
		// let google = this.google;

		// London ultra low emission zone extension end of 2019 / start of 2020
		this.ulez2020polygon = this.googleMap.definePolygon({
			"path" : ulez2020path,
			"strokeColor" : '#000000',
			"strokeOpacity" : 0,
			"strokeWeight" : 3,
			"fillColor" : '#7f7f7f',
			"fillOpacity" : 0.35
		});
	},

	"drawObscure" : function () {
		if ( consoleLog ) {console.log("ULEZControl.drawObscure"); }
		let google = this.google;

		let obscuringPath = [
			{ "lat" : 85.0, "lng" : -5.0 },
			{ "lat" : 5.0, "lng" : -5.0 },
			{ "lat" : 5.0, "lng" : 5.0 },
			{ "lat" : 85.0, "lng" : 5.0 }
		];

		this.obscure = new google.maps.Data();
		this.obscure.add({ "geometry" : new google.maps.Data.Polygon([obscuringPath,ulez2020path]) });
		this.obscure.setStyle({
			"strokeColor" : '#000000',
			"strokeOpacity" : 0,
			"strokeWeight" : 1,
			"fillColor" : '#000000',
			"fillOpacity" : 0.65
	    });

	    // this.obscure.setMap( this.googleMap._map );
	    // this.obscure.setMap( null );
	},

	"mapClickEvents" : function () {
		if ( consoleLog ) {console.log("ULEZControl.mapClickEvents"); }
		let that = this;

		// add click events for inside ulez2019
		this.ulez2019polygon.addListener('click', function(event){
			that.ulezPin(event.latLng, 'ULEZ2019');
		});
		// add click events for inside ulez2020
		this.ulez2020polygon.addListener('click', function(event){
			that.ulezPin(event.latLng, 'ULEZ2020');
		});

		// click events for rest of map
		this.googleMap.addMapEvent('click',function(data){
			that.ulezPin(data.latLng, 'map');
		});

		/*
		obscure.addListener('click',function(data){
			that.ulezPin(data.latLng, 'obscure');
		});
		*/
	},

	"ulezPin" : function (latLng, source) {
		if ( consoleLog ) {console.log("ULEZControl.ulezPin"); }
		let google = this.google;

		if ( consoleLog ) {console.log("lat: %s lng: %s source: '%s'", latLng.lat(), latLng.lng(), source); } 
		/*
		// function ulezPin(latLng){
			let pinName = "pinblack";
			let caption = "outside current ultra low emission zone and planned extension";
			let roads = 'black';
			let applyObscure = false;

			// set marker based on whether the click occurs inside or outside the ulez2019
			if( google.maps.geometry.poly.containsLocation(latLng,ulez2020polygon) ) {
				pinName = "pinwhite";
				caption = "inside planned 2020 extension to ultra low emission zone";
				roads = 'white';
				applyObscure = true;
			};

			if( google.maps.geometry.poly.containsLocation(latLng,ulez2019polygon) ) {
				pinName = "pinred";
				caption = "inside current ultra low emission zone";
				roads = 'red';
				applyObscure = true;
			};

			this.googleMap.placeMarker({
				"name" : pinName,
				"lat" : latLng.lat(),
				"lng" : latLng.lng(),
				"caption" : caption,
				"visible" : true
			});

			// change map style based on whether the click event point lies within a zone or not
			this.googleMap.setmapStyles(mapStyles[roads]);

			// display map overlay for clicks inside the ULEZ zones
			if (applyObscure) {
				obscure.setMap( this.googleMap._map );
			} else {
				obscure.setMap( null );
			}
			
		// }
		*/
	},

	/*
	"prepareRoute" : function () {
		if ( consoleLog ) {console.log("ULEZControl.prepareRoute"); }
		let google = this.google;

		// plot start and end points of journey
		// animate journey 10/20 steps
		// show indication when journey passed into current and extension zones
		// calculate location when the journey passes into current/extension zones

		// query route with start and end points: home, and stack
		// HertzStateController.js
		// https://developers.google.com/maps/documentation/javascript/reference/directions		
		this.ulezRoute = new GoogleRouter({
			"map" : this.googleMap,
			"steps" : 32,
			"delay" : 500, 
			"zones" : {
				"ulez2020" : this.ulez2020polygon,
				"ulez2019" : this.ulez2019polygon,
			}
		});
	},
	*/
	"liveRoute" : function (testJourneys) {
		if ( consoleLog ) {console.log("ULEZControl.liveRoute"); }
		// let google = this.google;

		this.journeys = [];
		let valid = true;

		if( typeof testJourneys === 'undefined') {
			// has the user supplied start and end points?
			if (!this.originReady) {
				valid = false;
			}

			if (this.destinationsReady.length === 0) {
				valid = false;
			}

			this.warningNoOrigin( !this.originReady );
			this.warningNoDestinations( this.destinationsReady.length === 0 );

			if (!valid)
				return false;

			this.googleMap.placeMarker({
				"name" : 'pinwhite',
				"lat" : this.origin.lat,
				"lng" : this.origin.lng,
				"caption" : this.origin.caption,
				"visible" : true
			});

			// build journeys array based on origin and destinations(s) fields
			for (var destination in this.destinations) {
			    if (Object.prototype.hasOwnProperty.call(this.destinations, destination)) {
			    	let data = this.destinations[destination];

					this.googleMap.placeMarker({
						"name" : 'pinred',
						"lat" : data.lat,
						"lng" : data.lng,
						"caption" : data.caption,
						"visible" : true
					});

			    	this.journeys.push({
						"title" : data.caption,
						"id" : destination,
						"origin" : this.origin,
						"destination" : data,
						"field" : destination.field 
			    	});
			    }
			}
		} else {
			this.googleMap.placeMarker({
				"name" : 'pingreen',
				"lat" : testJourneys[0].origin.lat,
				"lng" : testJourneys[0].origin.lng,
				"caption" : 'Start',
				"visible" : true
			});
			testJourneys.map(function(journey){
				this.googleMap.placeMarker({
					"name" : 'pinred',
					"lat" : journey.destination.lat,
					"lng" : journey.destination.lng,
					"caption" : journey.title,
					"visible" : true
				});
			},this);

			this.journeys = testJourneys;
		}

		if ( consoleLog ) { console.log( this.journeys ); }

		if ( this.journeys.length > 0 && (this.journeys.length - 1) < this.destinationCount) {

			this.routes = [];
			this.routers = [];
			let superThat = this;
			
			let queueParameters = {
				"queue" : this.journeys, // create clone of subset array
				"worker" : {
					"global" : {
						"delay" : superThat.delay
					},
					"action" : function(){
						let that = this;
						let workItem = that.settings.item;

						if ( consoleLog ) {console.log("worker started: '%s'", that.settings.item.id ); }

						// *******************************************************
						// query stored routes in /dist/json/test/
						// superThat.queryArchivedRoute(this, this.settings.item);
						// *******************************************************

						// *******************************************************
						// Live Google Directions API call
						superThat.queryDirectionsAPI(this, this.settings.item);
						// *******************************************************

					},
					"callback" : function(){
						this.queue.workerCompleted();
					}
				},
				"started" :  function(){
				},
				"finished" :  function(){
					
					// all route data collected
					// build map bounds
					superThat.buildMapBounds();

					superThat.processRouteData();
					// plot routes/points on map
				}
			};
			this.queue = new Queue(queueParameters);
			this.queue.startWorker();

			return;
		}

	},

	"queryArchivedRoute" : function (worker, journey) {
		if ( consoleLog ) {console.log("ULEZControl.queryArchivedRoute"); }
		let that = this;

		// dist/json/test/[ID].json

		if ( consoleLog ) {console.log("- id: '%s'", journey.id); }
		// load JSON stored route
			$.ajax({
				"type" : "GET",
				"url" : "dist/json/test/" + journey.id + ".json",
				"data" : {
				},
				"dataType" : "text",
				"cache" : false,
				"success" : function (text) {
					if ( consoleLog ) {console.log("ULEZControl.queryArchivedRoute success"); }

					let response = $.parseJSON(text);

					// massage JSON test data so that it closely resembles the google.directions query results
					let newRoute = {
						"origin" : {
							"lat" : parseFloat(response.origin.lat),
							"lng" : parseFloat(response.origin.lng)
						},
						"destination" : {
							"lat" : parseFloat(response.destination.lat),
							"lng" : parseFloat(response.destination.lng)
						},
						"distance" : parseInt(response.distance, 10),
						"field" : journey.field,
						"id" : journey.id,
						"title" : journey.title
					};

					// path[[lat,lng],...] -> nodes[LatLng,...]
					let nodes = [];
					response.path.map(function(node,index) {
						nodes.push(new google.maps.LatLng( parseFloat(node[0]), parseFloat(node[1]) ));
					}, this);
					newRoute.nodes = nodes;

					that.routes.push(newRoute);
					worker.completed();
				},
				"complete" : function () {
					if ( consoleLog ) {console.log("ULEZControl.queryArchivedRoute complete"); }
				}
			});

	},

	"queryDirectionsAPI" : function (worker, journey) {
		if ( consoleLog ) {console.log("ULEZControl.queryDirectionsAPI"); }
		let that = this;

		// LIVE GOOGLE API ACCESS
		// be wary of quotas / access limits
		this.directionsService.route({
			"origin" : new google.maps.LatLng( journey.origin.lat, journey.origin.lng ),
			"destination" :  new google.maps.LatLng( journey.destination.lat, journey.destination.lng ),
			"travelMode" : "DRIVING",
			"language" : "en"
		},
		function(response, status) {
			if (status === 'OK') {

				let routeLength = response.routes[0].legs[0].distance.value;
				let nodes = response.routes[0].overview_path;
				if ( consoleLog ) {console.log("route length: %skm nodes: %s", (routeLength / 1000), nodes.length); } 

				journey.distance = routeLength;
				journey.nodes = nodes;
				that.routes.push(journey);

				worker.completed();	

			} else {
				// inability to find route
			}
		});
	},

	"buildMapBounds" : function () {
		if ( consoleLog ) {console.log("ULEZControl.buildMapBounds"); }
		let that = this;
		let google = this.google;

		let bounds = new google.maps.LatLngBounds();

		// iterate over routes
		this.routes.map(function(route,index) {
			bounds.extend(new google.maps.LatLng( route.origin.lat, route.origin.lng ));
			bounds.extend(new google.maps.LatLng( route.destination.lat, route.destination.lng ));

			// iterate over nodes in route
			if ( consoleLog ) {console.log( "nodes: %s", route.nodes.length ); }
			route.nodes.map(function(node,index) {
				bounds.extend(node);
			}, this);
		}, this);
		this.googleMap._map.fitBounds(bounds);

	},

	"processRouteData" : function () {
		if ( consoleLog ) {console.log("ULEZControl.processRouteData"); }
		let that = this;

		this.routes.map(function(route,index) {
			let router = new GoogleRouter2({
				"controller" : this,
				"map" : this.googleMap,
				"steps" : this.segmentCount,
				"delay" : this.segmentInterval, 
				"zones" : {
					"ulez2020" : this.ulez2020polygon,
					"ulez2019" : this.ulez2019polygon,
				}
			}); 
			router.route(route);
			// this.routers.push(router);
		}, this);
	},

	"findBoundsManually" : function () {
		latMin = nodes[0].lat;
		latMax = nodes[0].lat;
		lngMin = nodes[0].lng;
		lngMax = nodes[0].lng;

		nodes.map(function(node){
			if ( consoleLog ) {console.log(node.lat,node.lng); } 
			if (latMin > node.lat) { latMin = node.lat; } else if (latMax < node.lat) { latMax = node.lat; }
			if (lngMin > node.lng) { lngMin = node.lng; } else if (lngMax < node.lng) { lngMax = node.lng; }

		}, this);

		if ( consoleLog ) {console.log("latMin(S):[%s] latMax(N):[%s] lngMin:[%s] lngMax:[%s]", latMin, latMax, lngMin, lngMax); } 
		return [latMin, latMax, lngMin, lngMax];
	},

	"showWarning" : function (field) {
		if ( consoleLog ) {console.log("ULEZControl.showWarning field: '%s'", field); }
		$('div.ulezCharge').addClass('open');
		if (field !== '') {
			$('#' + field).parents('.formField').eq(0).addClass('warning');	
		}
	},

	"warningNoOrigin" : function(state){
		if ( consoleLog ) {console.log("ULEZControl.showWarningNoOrigin"); }

		// state = true, show error
		if (state) {
			$('div.noOrigin').addClass('open');
		} else {
			$('div.noOrigin').removeClass('open');
		}

		
	},

	"warningNoDestinations" : function(state){
		if ( consoleLog ) {console.log("ULEZControl.showWarningNoDestinations"); }

		// state = true, show error
		if (state) {
			$('div.noDestinations').addClass('open');
		} else {
			$('div.noDestinations').removeClass('open');
		}
	},

	"connect" : function (message) {
		if ( consoleLog ) {console.log("ULEZControl.connect '%s'", message); }
	}
}

export default ULEZControl;