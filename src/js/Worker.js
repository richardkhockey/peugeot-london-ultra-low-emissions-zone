/* ------------------------------------------------ */
// Worker
/* ------------------------------------------------ */

function Worker(parameters) {
	this.parameters = parameters;

	this.settings = parameters.settings;
	this.queue = parameters.queue; // link back to parent queue object
	this.action = parameters.action;
	this.callback = parameters.callback;

	this.start();
}

Worker.prototype = {
	"constructor" : Worker,
	"template" : function () { var that = this; },
	
	"start" : function () {
		var that = this;

		this.action(this);
	},
	"completed" : function () {
		var that = this;

		this.callback(this, this.queue);
	}
};

// window.Worker = Worker;
export default Worker;