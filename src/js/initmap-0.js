	let stackoffice = new google.maps.LatLng( 51.5218891, -0.1359421 ); // 90 tottenham court road lat & lng

	let map = new google.maps.Map(document.querySelector('.googlemap'), {
		"zoom" : 17,
		"center" : {
		  "lat" : 51.5218891,
		  "lng" : -0.1359421
		},
		"panControl" : true,
		"zoomControl" : true,
		"mapTypeControl" : true,
		"streetViewControl" : true,
		"clickableIcons" : false
	});


	// tilesloaded
	map.addListener('tilesloaded', function(me) {
	    // let delay = window.setTimeout(function(){
	    	$('#stackmap').addClass('ready');


	    // }, 10);
	});

	map.addListener('click', function(point) {
		// me = google.maps.MouseEvent
		// me.latLng = google.maps.LatLng
		// [ me.latLng.lat(), me.latLng.lng() ]

		console.log("lat: %s lng: %s", point.latLng.lat(), point.latLng.lng());
		let clickMarker = new google.maps.Marker({
			"position" : point.latLng,
			"map" : map,
			"title" : "lat: " + point.latLng.lat() + " lng: " + point.latLng.lng()
		});
		/*
		points.push(clickMarker);

		po = (po !== '') ? po + "," : po;
		po = po  + "{\"lat\" : " + me.latLng.lat() + ", \"lng\" : " + me.latLng.lng() + "}";
		$('#path').empty().append(po);

		console.log("index: %s start lat: %s lng: %s", pointIndex, me.latLng.lat(),me.latLng.lng());
		pointIndex++;
		*/
	});

	/*
	// add 'STACK' marker to map	
	// define custom 'STACK' icon
	let stackIcon = {
		"url" : "dist/img/drop_pin_02.png",
		"anchor" : new google.maps.Point(33,80),
		"size" : new google.maps.Size(331,400),
		"scaledSize" : new google.maps.Size(66,80)
	};

	let newMarker = new google.maps.Marker({
      "map" : map,
      "position" : stackoffice,
      "icon" : stackIcon,
      "animation" : google.maps.Animation.DROP
    });
	*/

	/*
    // kml overlay for low emission zone / ultra low emission zone
    let kmlLayer = new google.maps.KmlLayer( ulezKML, {
      suppressInfoWindows: true,
      preserveViewport: true,
      map: map
    });

    // need to edd specific click events for KML layers as they do not pass clicks through to the map
	kmlLayer.addListener('click', function(kmlEvent) {
		console.log("kmlEvent lat: %s lng: %s", kmlEvent.latLng.lat(), kmlEvent.latLng.lng());

		let clickMarker = new google.maps.Marker({
			"position" : kmlEvent.latLng,
			"map" : map,
			"title" : "lat: " + kmlEvent.latLng.lat() + " lng: " + kmlEvent.latLng.lng()
		});
	});
	*/

	// load GeoJSON layer
	// map.data.loadGeoJson(ulezGeoJSON);

    /* image overlay for ultra low emission overlay */
    /*
	var imageBounds = {
		north: 40.773941,
		south: 40.712216,
		east: -74.12544,
		west: -74.22655
	};

	historicalOverlay = new google.maps.GroundOverlay( 'https://www.lib.utexas.edu/maps/historical/newark_nj_1922.jpg', imageBounds );
	historicalOverlay.setMap(map);
	*/

	// define polygon object for ulez2019
	var ulez2019Polygon = new google.maps.Polygon({
		"paths" : ulez2019path,
		"strokeColor" : '#FF0000',
		"strokeOpacity" : 0.8,
		"strokeWeight" : 3,
		"fillColor" : '#FF0000',
		"fillOpacity" : 0.35
	});
	ulez2019Polygon.setMap(map);
	ulez2019Polygon.addListener('click', function(event){
		console.log("event lat: %s lng: %s", event.latLng.lat(), event.latLng.lng());

		let clickMarker = new google.maps.Marker({
			"position" : event.latLng,
			"map" : map,
			"title" : "event lat: " + event.latLng.lat() + " lng: " + event.latLng.lng()
		});
	});
