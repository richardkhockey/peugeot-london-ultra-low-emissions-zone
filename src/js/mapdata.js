let lezKML = 'https://production.stackworks.com/low-emission-zone/kml/londonlowemissionzone.kml';
let ulezKML = 'https://production.stackworks.com/low-emission-zone/kml/ulez2019.kml';

let lezGeoJSON = 'https://production.stackworks.com/low-emission-zone/json/londonlowemissionzone.json';
let ulezGeoJSON = 'https://production.stackworks.com/low-emission-zone/json/ulez2019.geojson';

let thisGoogleMap;
let ulez2019polygon;
let ulez2020polygon;

let mapStyles = {
	"red" : [
		{
			"featureType": "road",
			"stylers": [
				{
					"color": "#ff0000"
				}
			]
		}
	],
	"white" : [
		{
			"featureType": "road",
			"stylers": [
				{
					"color": "#ffffff"
				}
			]
		}
	],
	"black" : [
		{
			"featureType": "road",
			"stylers": [
				{
					"color": "#000000"
				}
			]
		}
	]
};

// { "lat" : 51.5240183, "lng" : 0.0726507 },
let obscuringPath = [
	{ "lat" : 85.0, "lng" : -5.0 },
	{ "lat" : 5.0, "lng" : -5.0 },
	{ "lat" : 5.0, "lng" : 5.0 },
	{ "lat" : 85.0, "lng" : 5.0 }
]
let obscure;
