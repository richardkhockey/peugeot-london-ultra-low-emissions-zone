import './styles.scss'
import Bootstrap from './js/bootstrap'
import PageSetup from './js/PageSetup'

import ULEZControl from './js/ULEZControl.js'

PageSetup();

window.consoleLog = ( window.location.href.indexOf('debug=1') !== -1 ); // enable console log debug messages
window.testJourneys = ( window.location.href.indexOf('testdata=1') !== -1 ); // enable test journey data
window.archiveJourneys = ( window.location.href.indexOf('archive=1') !== -1 ); // archive journey data

let controller;
let supportsGeolocation;

/*
prepopulate form from querystring using js
postcode "E18%20@nJ" -> #origin
vehreg "W12%20NH2" -> #registration
compliant "yes" / "no" -> #compliant
*/

// google maps loaded and ready for use
function initmap(){

	// $('#origin').val('E18 2NJ'); // attach geocode to this to set origin for all journeys
	
	// attach autocomplete to 3 destination fields

	$('#compliant').val('yes');
	$('#registration').val('W12 NH2');
	// may need to change text content displayed based on these values
	/*
	let supportsGeolocation = ("geolocation" in navigator);
	if ( supportsGeolocation ) {
		navigator.geolocation.getCurrentPosition(function(position) {
			console.log("geolocation supported : lat [%s] lng[%s]", position.coords.latitude, position.coords.longitude);

		});
	}
	*/

	controller = new ULEZControl({
		"google" : google, // pass google object into controller
		"apikey" : "AIzaSyBBvYQD3pGGYKrinw2QAXODQZ919slgNBY",
		"container" : ".googlemap",
		"segmentCount" : 16, // journey broken into 16 segments
		"segmentInterval" : 250, // delay between drawing each journey segment in milliseconds
		"destinationCount" : 5
	});

	// set default map centre (London, UK) and zoom level
	controller.drawMap({
		"zoom" : 11,
		"center" : {"lat" : 51.513264368977296, "lng" : -0.11471673956293671}
	});
	
	// inner and outer low emission zones
	controller.generateULEZ2019();
	controller.generateULEZ2020();
	// controller.drawObscure();

	// controller.mapClickEvents();

	// bind google autocompleters to text fields for origin and destinations
	controller.bindAutocompleteFields();

	$('#go3').on('click',function (e) {
		e.preventDefault();

		if ( window.testJourneys ) {
			// test data, skips google autocomplete fields
			// append 'testdata=1' to URL to trigger
			// sample routes	
			// id field matches filename of archived json file in dist/json/test/
			// id 'stratford-westfield' -> dist /json/test/stratford-westfield.json

			let home = { "lat" : 51.59772129317902, "lng" : 0.018531116087388 }; // 68 Buckingham Road E18 2NJ (1981 - present)
			let _163_Belgrave_Road = { "lat" : 51.5762191, "lng" : -0.0180605 }; // 163 Belgrave Road E17 (1970 - 1981)
			let stack = { "lat" : 51.5218891, "lng" : -0.1359421 }; // 90 tottenham court road W1T4TJ
			let electricBallroom = { "lat" : 51.5396927940899, "lng" : -0.14311422393814155 }; // Electric Ballroom, Camden
			let threeColts = { "lat" : 51.62434721514754, "lng" : 0.04293015858365834 }; // The Three Colts, Buckhurst Hill
			let stratford = { "lat" : 51.54235316179687, "lng" : -0.0023549752332883145 }; // Stratford Westfield
			let slimelight = { "lat" : 51.532536834526844, "lng" : -0.10499713816955136 }; // The Slimelight @ Elektrowerks Angel, Islington
			let plymouth = { "lat" : 50.3754565, "lng" : -4.14265649999993 };
			let norwich = { "lat" : 52.6308859, "lng" : 1.2973550000000387 };
			let dartmouth = { "lat" : 50.352517, "lng" : -3.5788069999999834 };
			let hull = { "lat" : 53.76762360000001 , "lng" : -0.3274198000000297 };
			let gwithian = { "lat" : 50.221352 , "lng" : -5.386339799999973 }; // Gwithian, Hayle, North Cornwall coast
			let kilgetty = { "lat" : 51.731792, "lng" : -4.717630999999983 }; // Kilgetty, West Wales

			controller.liveRoute([
				{
					"title" : "Plymouth",
					"id" : "plymouth",
					"origin" : home,
					"destination" : plymouth,
					"field" : "place1" 
				},
				{
					"title" : "Dartmouth",
					"id" : "dartmouth",
					"origin" : home,
					"destination" : dartmouth,
					"field" : "place2" 
				},
				{
					"title" : "Norwich",
					"id" : "norwich",
					"origin" : home,
					"destination" : norwich,
					"field" : "place3" 
				}
			]);
			/*
			controller.liveRoute([
				{
					"title" : "Stratford Westfield",
					"id" : "stratford-westfield",
					"origin" : home,
					"destination" : stratford,
					"field" : "place1" 
				},
				{
					"title" : "The Electric Ballroom, Camden",
					"id" : "electric-ballroom",
					"origin" : home,
					"destination" : electricBallroom, 
					"field" : "place2" 
				},
				{
					"title" : "STACK",
					"id" : "stack",
					"origin" : home,
					"destination" : stack, 
					"field" : "place3" 
				},
				{
					"title" : "The Slimelight @ Elektrowerks Angel, Islington",
					"id" : "slimelight",
					"origin" : home,
					"destination" : slimelight,
					"field" : "" 
				},
				{
					"title" : "The Three Colts, Buckhurst Hill",
					"id" : "threecolts",
					"origin" : home,
					"destination" : threeColts,
					"field" : "" 
				}
			]);
			*/
		} else {
			// reads origin and destinations from text field/autocomplete
			// will not display routes till an origin and at least one destination have been selected
			controller.liveRoute();
		}
	});
}
window.initmap = initmap;

$(document).ready(function () {
	/* ---------------------------------------------------------------------------- */


	// useful stuff goes here


	/* ---------------------------------------------------------------------------- */
});
