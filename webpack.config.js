const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractCSS = new ExtractTextPlugin('[name].bundle.css')
const ModernizrWebpackPlugin = require('modernizr-webpack-plugin');

const modernizrConfig = {
  "minify": true,
  "options": [
    "addTest",
    "setClasses"
  ],
  "feature-detects": [
    "test/css/mediaqueries",
    "test/css/multiplebgs",
    "test/css/lastchild",
    "test/css/nthchild",
    "test/css/opacity",
    "test/css/animations",
    "test/css/transforms",
    "test/css/transforms3d",
    "test/css/transformslevel2",
    "test/css/transformstylepreserve3d",
    "test/css/transitions",
    "test/css/vhunit",
    "test/css/vmaxunit",
    "test/css/vminunit",
    "test/css/vwunit",
    "test/css/supports"
  /*
    "test/svg",
    "test/video",
    "test/webanimations",
    "test/webgl",
    "test/css/backgroundblendmode",
    "test/css/backgroundcliptext",
    "test/css/backgroundposition-shorthand",
    "test/css/backgroundposition-xy",
    "test/css/backgroundrepeat",
    "test/css/backgroundsize",
    "test/css/backgroundsizecover",
    "test/css/borderradius",
    "test/css/boxshadow",
    "test/css/boxsizing",
    "test/css/calc",
    "test/css/displaytable",
    "test/css/ellipsis",
    "test/css/filters",
    "test/css/flexbox",
    "test/css/flexboxlegacy",
    "test/css/flexboxtweener",
    "test/css/fontface",
    "test/css/generatedcontent",
    "test/css/gradients",
    "test/css/overflow-scrolling",
    "test/css/rgba",
    "test/css/target",
    "test/css/textshadow",
    "test/svg/asimg",
    "test/svg/clippaths",
    "test/svg/filters",
    "test/svg/foreignobject",
    "test/svg/inline",
    "test/svg/smil"
   */
  ]
}

const config = {
  context: path.resolve(__dirname, 'src'),
  entry: { "system" : './system.js' },
	resolve: {
		extensions: ['.js'],
		alias: {
			'jquery': __dirname + '/node_modules/jquery/src/jquery.js',
		}
	},
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
    {
      test: /\.scss$/,
      loader: extractCSS.extract(['css-loader','sass-loader'])
    },
    {
      test: /\.js$/,
      include: path.resolve(__dirname, 'src'),
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [
            ['es2015', { modules: false }]
          ]
        }
      }]
    },
    {
      test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      use: 'url-loader?limit=10000',
    },
    {
      test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
      use: 'file-loader',
    },
    ]
  },
  plugins: [
    extractCSS,
    new webpack.ProvidePlugin({
    	$: 'jquery',
    	jQuery: 'jquery',
    }),
    new ModernizrWebpackPlugin(modernizrConfig),
    new webpack.optimize.UglifyJsPlugin({minimize: true})
  ]
}

module.exports = config